# Sales Taxes
Basic sales tax is applicable at a rate of 10% on all goods, except books, food, and medical products that
are exempt. Import duty is an additional sales tax applicable on all imported goods at a rate of 5%, with no
exemptions.
When I purchase items, I receive a receipt which lists the name of all the items and their price (including tax),
finishing with the total cost of the items, and the total amounts of sales taxes paid. The rounding rules for
sales tax are that for a tax rate of n%, a shelf price of p contains (np/100 rounded up to the nearest 0.05)
amount of sales tax.
Write an application that returns the receipt details for these shopping baskets...

## Input
Input 1:

1 book at 12.49

1 music CD at 14.99

1 chocolate bar at 0.85

Input 2:

1 imported box of chocolates at 10.00

1 imported bottle of perfume at 47.50

Input 3:

1 imported bottle of perfume a t 27.99

1 bottle of perfume at 18.99

1 packet of headache pills at 9.75

1 box of imported chocolates at 11.25


## OUTPUT
Output 1:

1 book : 12.49

1 music CD: 16.49

1 chocolate bar : 0.85

Sales Taxes : 1.50 Total : 29.83

Output 2:

1 imported box of chocolates : 10.50

1 imported bottle of perfume : 54.65

Sales Taxes : 7.65 Total : 65.15

Output 3:

1 imported bottle of perfume : 32.19

1 bottle of perfume : 20.89

1 packet of headache pills : 9.75

1 imported box of chocolates : 11.85

Sales Taxes = 6.70 Total = 74.68

## Solution

## Implementation Approach
Two different types of design patterns for this kind of problem can be applied:
 - Abstract Factory
 - Strategy

## Abstract Factory Pattern
The Product is an Abstract Super Class and different kinds of items/products like book, chocolates, food, etc. will inherit from the Product class. It will give the facility to extend the new Product type in the future.

Here, the main reason to separate classes for each product type is that other attributes can be added in the future by just extending the Product class.

Below are classes that can be implemented:

Abstract Factory → ProductFactory
Concrete Factory → BookProductFactory, FoodProductFactory, MedicineProductFactory
Abstract Product → Product
Concrete Product → BookProduct, FoodProduct, MedicineProduct
Client → StoreShelf

- Advantages:

If a new product will be added, then there will be no need to change the existing client code.

## Strategy Pattern
Tax Calculation will be designed by using Strategy Patterns.

Below are classes that will be involved:

Strategy → ITaxCalculator
Concrete Strategy → LocalTaxCalculator, or any tax strategies like import tax, custom tax, etc.
Context → Biller

- Advantages:

We can easily add a new algorithm for Tax Calculation in the feature and this can be selected at run time. Each product has a different tax percentage and rules. So, strategy can be useful for each category.
The actual creation of a TaxCalculator will be delegated to the Factory method.