package com.risf.salestaxes;

import com.risf.salestaxes.service.SalesTaxService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SalesTaxesApplicationTests {

	@InjectMocks
	SalesTaxService salesTaxService;


	@Test
	void testOutput1() {
		salesTaxService.shoppingBasketInput1();
		assertThat(salesTaxService.getReceipt().getTotalSalesTax()).isEqualTo(1.50);
		assertThat(salesTaxService.getReceipt().getTotalAmount()).isEqualTo(29.83);
	}


	@Test
	void testOutput2() {
		salesTaxService.shoppingBasketInput2();
		assertThat(salesTaxService.getReceipt().getTotalSalesTax()).isEqualTo(7.65);
		assertThat(salesTaxService.getReceipt().getTotalAmount()).isEqualTo(65.15);
	}


	@Test
	void testOutput3() {
		salesTaxService.shoppingBasketInput3();
		assertThat(salesTaxService.getReceipt().getTotalSalesTax()).isEqualTo(6.70);
		assertThat(salesTaxService.getReceipt().getTotalAmount()).isEqualTo(74.68);
	}


}
