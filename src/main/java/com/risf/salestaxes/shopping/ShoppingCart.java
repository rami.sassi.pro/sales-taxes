package com.risf.salestaxes.shopping;

import com.risf.salestaxes.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> productList;

    public ShoppingCart() {
        productList = new ArrayList<>();
    }

    public void addItemToCart(Product product) {
        productList.add(product);
    }

    public List<Product> getItemsFromCart() {
        return productList;
    }

    public int getCartSize() {
        return productList.size();
    }
}
