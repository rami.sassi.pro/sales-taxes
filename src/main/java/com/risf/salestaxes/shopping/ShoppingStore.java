package com.risf.salestaxes.shopping;

import com.risf.salestaxes.billing.PaymentCounter;
import com.risf.salestaxes.billing.Receipt;
import com.risf.salestaxes.products.Product;

import java.util.Scanner;

public class ShoppingStore {
    private ShoppingCart shoppingCart;
    private StoreShelf storeShelf;
    private PaymentCounter paymentCounter;
    private String country;

    public ShoppingStore() {
        country = "Local";
        shoppingCart = new ShoppingCart();
        paymentCounter = new PaymentCounter(country);
        storeShelf = new StoreShelf();
    }

    public void retrieveOrderAndPlaceInCart(String name, double price, boolean imported, int quantity, boolean exempted) {
        Product product = storeShelf.searchAndRetrieveItemFromShelf(name, price, imported, quantity, exempted);
        shoppingCart.addItemToCart(product);
    }

    public void getSalesOrder() {
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Enter the product name:");
            String name = scanner.nextLine();

            System.out.println("Enter the product price:");
            double price = Double.parseDouble(scanner.nextLine());

            System.out.println("Is product imported or not? (Y/N)");
            boolean imported = scanner.nextLine().equalsIgnoreCase("Y");

            System.out.println("Enter the quantity:");
            int quantity = Integer.parseInt(scanner.nextLine());

            System.out.println("Is product exempted or not? (Y/N)");
            boolean exempted = scanner.nextLine().equalsIgnoreCase("Y");

            retrieveOrderAndPlaceInCart(name, price, imported, quantity, exempted);
        } while (isAddAnotherProduct());
    }

    public Receipt checkOut() {
        paymentCounter.billItemsInCart(shoppingCart);
        Receipt receipt = paymentCounter.getReceipt();
        paymentCounter.printReceipt(receipt);

        return receipt;
    }

    public boolean isAddAnotherProduct() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Do you want to add another Product? (Y/N)");

            String input = scanner.nextLine();
            if (input.equalsIgnoreCase("Y")) {
                return true;
            } else if (input.equalsIgnoreCase("N")) {
                return false;
            } else {
                System.out.println("Invalid input. Enter (Y/N)");
            }
        }
    }
}
