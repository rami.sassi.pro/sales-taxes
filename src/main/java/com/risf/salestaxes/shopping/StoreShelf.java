package com.risf.salestaxes.shopping;

import com.risf.salestaxes.products.*;

import java.util.HashMap;
import java.util.Map;

public class StoreShelf {

    private final Map<String, Product> productItems;

    public StoreShelf() {
        productItems = new HashMap<>();
        addProductItemsToShelf("book", new BookProduct());
        addProductItemsToShelf("music cd", new MiscellaneousProduct());
        addProductItemsToShelf("chocolate bar", new FoodProduct());
        addProductItemsToShelf("box of chocolates", new FoodProduct());
        addProductItemsToShelf("bottle of perfume", new MiscellaneousProduct());
        addProductItemsToShelf("packet of headache pills", new MedicalProduct());
    }

    public void addProductItemsToShelf(String productItem, Product productCategory) {
        productItems.put(productItem, productCategory);
    }

    public Product searchAndRetrieveItemFromShelf(String name, double price, boolean imported, int quantity, boolean exempted) {
        Product productItem = productItems.get(name).getFactory().createProduct(name, price, imported, quantity, exempted);
        return productItem;
    }

    public int getShelfSize() {
        return productItems.size();
    }
}
