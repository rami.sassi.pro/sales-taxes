package com.risf.salestaxes.productFactories;

import com.risf.salestaxes.products.BookProduct;
import com.risf.salestaxes.products.Product;

public class BookProductFactory extends ProductFactory {
    @Override
    public Product createProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        return new BookProduct(name, price, imported, quantity, exempted);
    }
}
