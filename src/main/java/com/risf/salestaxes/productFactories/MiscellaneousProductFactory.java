package com.risf.salestaxes.productFactories;

import com.risf.salestaxes.products.MiscellaneousProduct;
import com.risf.salestaxes.products.Product;

public class MiscellaneousProductFactory extends ProductFactory {
    @Override
    public Product createProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        return new MiscellaneousProduct(name, price, imported, quantity, exempted);
    }
}
