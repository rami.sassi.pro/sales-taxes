package com.risf.salestaxes.productFactories;

import com.risf.salestaxes.products.FoodProduct;
import com.risf.salestaxes.products.Product;

public class FoodProductFactory extends ProductFactory {
    @Override
    public Product createProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        return new FoodProduct(name, price, imported, quantity, exempted);
    }
}
