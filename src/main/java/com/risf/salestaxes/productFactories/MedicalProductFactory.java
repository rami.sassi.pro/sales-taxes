package com.risf.salestaxes.productFactories;

import com.risf.salestaxes.products.MedicalProduct;
import com.risf.salestaxes.products.Product;

public class MedicalProductFactory extends ProductFactory {
    @Override
    public Product createProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        return new MedicalProduct(name, price, imported, quantity, exempted);
    }
}
