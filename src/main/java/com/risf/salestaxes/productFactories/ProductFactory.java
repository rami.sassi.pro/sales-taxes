package com.risf.salestaxes.productFactories;

import com.risf.salestaxes.products.Product;

public abstract class ProductFactory {
    public abstract Product createProduct(String name, double price, boolean imported, int quantity, boolean exempted);
}
