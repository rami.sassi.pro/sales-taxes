package com.risf.salestaxes.products;

import com.risf.salestaxes.productFactories.ProductFactory;

public abstract class Product {

    protected String name;
    private double price;
    private boolean imported;
    private int quantity;

    private boolean exempted;
    private double taxedCost;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isExempted() {
        return exempted;
    }

    public void setExempted(boolean exempted) {
        this.exempted = exempted;
    }

    public double getTaxedCost() {
        return taxedCost;
    }

    public void setTaxedCost(double taxedCost) {
        this.taxedCost = taxedCost;
    }

    public Product() {
        this.name = "";
        this.price = 0.0;
        this.imported = false;
        this.quantity = 0;
        this.exempted = false;
        this.taxedCost = 0.0;
    }

    public Product(String name, double price, boolean imported, int quantity, boolean exempted) {
        this.name = name;
        this.price = price;
        this.imported = imported;
        this.quantity = quantity;
        this.exempted = exempted;
    }

    @Override
    public String toString() {
        return (quantity + " " + importedToString(imported) + " " + name  + " : " + taxedCost);
    }

    public String importedToString(boolean imported) {
        if (!imported)
            return "";
        else
            return "imported";
    }

    public abstract ProductFactory getFactory();

    public abstract double getTaxValue(String country);
}