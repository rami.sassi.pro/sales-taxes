package com.risf.salestaxes.products;

import com.risf.salestaxes.productFactories.MedicalProductFactory;
import com.risf.salestaxes.productFactories.ProductFactory;
import com.risf.salestaxes.taxCalculations.LocalTaxValues;

public class MedicalProduct extends Product {

    public MedicalProduct() {
        super();
    }

    public MedicalProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        super(name, price, imported, quantity, exempted);
    }

    @Override
    public ProductFactory getFactory() {
        return new MedicalProductFactory();
    }

    @Override
    public double getTaxValue(String country) {
        if (country.equals("Local")) {
            return LocalTaxValues.MEDICAL_TAX;
        } else {
            return 0;
        }
    }
}
