package com.risf.salestaxes.products;

import com.risf.salestaxes.productFactories.BookProductFactory;
import com.risf.salestaxes.productFactories.ProductFactory;
import com.risf.salestaxes.taxCalculations.LocalTaxValues;

public class BookProduct extends Product {

    public BookProduct() {
        super();
    }

    public BookProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        super(name, price, imported, quantity, exempted);
    }

    @Override
    public ProductFactory getFactory() {
        return new BookProductFactory();
    }

    @Override
    public double getTaxValue(String country) {
        if (country.equals("Local")) {
            return LocalTaxValues.BOOK_TAX;
        } else {
            return 0;
        }
    }
}
