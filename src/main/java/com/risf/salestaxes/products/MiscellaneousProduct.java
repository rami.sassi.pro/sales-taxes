package com.risf.salestaxes.products;

import com.risf.salestaxes.productFactories.MiscellaneousProductFactory;
import com.risf.salestaxes.productFactories.ProductFactory;
import com.risf.salestaxes.taxCalculations.LocalTaxValues;

public class MiscellaneousProduct extends Product {

    public MiscellaneousProduct() {
        super();
    }

    public MiscellaneousProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        super(name, price, imported, quantity, exempted);
    }

    @Override
    public ProductFactory getFactory() {
        return new MiscellaneousProductFactory();
    }

    @Override
    public double getTaxValue(String country) {
        if (country.equals("Local")) {
            return LocalTaxValues.MISC_TAX;
        } else {
            return 0;
        }
    }
}
