package com.risf.salestaxes.products;

import com.risf.salestaxes.productFactories.FoodProductFactory;
import com.risf.salestaxes.productFactories.ProductFactory;
import com.risf.salestaxes.taxCalculations.LocalTaxValues;

public class FoodProduct extends Product {

    public FoodProduct() {
        super();
    }

    public FoodProduct(String name, double price, boolean imported, int quantity, boolean exempted) {
        super(name, price, imported, quantity, exempted);
    }

    @Override
    public ProductFactory getFactory() {
        return new FoodProductFactory();
    }

    @Override
    public double getTaxValue(String country) {
        if (country.equals("Local")) {
            return LocalTaxValues.FOOD_TAX;
        } else {
            return 0;
        }
    }
}