package com.risf.salestaxes.utils;

public class TaxUtil {

    private static final double ROUND_OFF = 0.05;

    /**
     * Rounds off a double value to the nearest 0.05
     *
     * @param value The value to be rounded off.
     * @return The rounded off value.
     */
    public static double roundOff(double value) {
        return Math.round(value / ROUND_OFF) * ROUND_OFF;
    }

    /**
     * Truncates a double value to two decimal places.
     *
     * @param value The value to be truncated.
     * @return The truncated value.
     */
    public static double truncate(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    /**
     * Parses a character into a boolean value.
     *
     * @param value The character to be parsed ('Y' for true, 'N' for false).
     * @return The parsed boolean value.
     */
    public static boolean parseBoolean(char value) {
        return (value == 'Y' || value == 'y');
    }
}

