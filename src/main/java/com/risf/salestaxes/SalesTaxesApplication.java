package com.risf.salestaxes;

import com.risf.salestaxes.shopping.ShoppingStore;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesTaxesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesTaxesApplication.class, args);

		ShoppingStore store = new ShoppingStore();
		store.getSalesOrder();
		store.checkOut();

	}

}
