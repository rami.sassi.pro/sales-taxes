package com.risf.salestaxes.service;

import com.risf.salestaxes.billing.PaymentCounter;
import com.risf.salestaxes.billing.Receipt;
import com.risf.salestaxes.products.Product;
import com.risf.salestaxes.shopping.ShoppingCart;
import com.risf.salestaxes.shopping.StoreShelf;

public class SalesTaxService {
    private ShoppingCart shoppingCart;
    private StoreShelf storeShelf;
    private PaymentCounter paymentCounter;
    private String country;


    public SalesTaxService() {
        country = "Local";
        shoppingCart = new ShoppingCart();
        paymentCounter = new PaymentCounter(country);
        storeShelf = new StoreShelf();
    }

    public void shoppingBasketInput1() {
        retrieveOrderAndPlaceInCart("book", 12.49, false, 1, true);
        retrieveOrderAndPlaceInCart("music cd", 14.99, false, 1, false);
        retrieveOrderAndPlaceInCart("chocolate bar", 0.85, false, 1, true);
    }

    public void shoppingBasketInput2() {
        retrieveOrderAndPlaceInCart("box of chocolates", 10.00, true, 1, true);
        retrieveOrderAndPlaceInCart("bottle of perfume", 47.50, true, 1, false);
    }

    public void shoppingBasketInput3() {
        retrieveOrderAndPlaceInCart("bottle of perfume", 27.99, true, 1, false);
        retrieveOrderAndPlaceInCart("bottle of perfume", 18.99, false, 1, false);
        retrieveOrderAndPlaceInCart("packet of headache pills", 9.75, false, 1, true);
        retrieveOrderAndPlaceInCart("box of chocolates", 11.25, true, 1, true);
    }

    public void retrieveOrderAndPlaceInCart(String name, double price, boolean imported, int quantity, boolean isExempt) {
        Product product = storeShelf.searchAndRetrieveItemFromShelf(name, price, imported, quantity, isExempt);
        shoppingCart.addItemToCart(product);
    }

    public Receipt getReceipt() {
        paymentCounter.billItemsInCart(shoppingCart);
        Receipt receipt = paymentCounter.getReceipt();
        paymentCounter.printReceipt(receipt);

        return receipt;
    }

}
