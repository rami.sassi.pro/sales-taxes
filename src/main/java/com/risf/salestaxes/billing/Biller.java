package com.risf.salestaxes.billing;

import com.risf.salestaxes.products.Product;
import com.risf.salestaxes.taxCalculations.ITaxCalculator;
import com.risf.salestaxes.utils.TaxUtil;

import java.util.List;

public class Biller {

    private ITaxCalculator taxCalculator;

    public Biller(ITaxCalculator taxCalc) {
        taxCalculator = taxCalc;
    }

    public double calculateTax(double price, double tax, boolean imported, boolean exempted) {
        double totalProductTax = taxCalculator.calculateTax(price, tax, imported, exempted);
        return totalProductTax;
    }

    public double calcTotalProductCost(double price, double tax) {
        return TaxUtil.truncate(price + tax);
    }

    public double calcTotalTax(List<Product> prodList) {
        double totalTax = 0.0;

        for (Product Product : prodList) {
            totalTax += (Product.getTaxedCost() - Product.getPrice());
        }

        return TaxUtil.truncate(totalTax);
    }

    public double calcTotalAmount(List<Product> prodList) {
        double totalAmount = 0.0;

        for (Product product : prodList) {
            totalAmount += product.getTaxedCost();
        }

        return TaxUtil.truncate(totalAmount);
    }

    public Receipt createNewReceipt(List<Product> productList, double totalTax, double totalAmount) {
        return new Receipt(productList, totalTax, totalAmount);
    }

    public void generateReceipt(Receipt r) {
        String receipt = r.toString();
        System.out.println(receipt);
    }
}
