package com.risf.salestaxes.billing;

import com.risf.salestaxes.products.Product;
import com.risf.salestaxes.shopping.ShoppingCart;
import com.risf.salestaxes.taxCalculations.ITaxCalculator;
import com.risf.salestaxes.taxCalculations.TaxCalculatorFactory;

import java.util.List;

public class PaymentCounter {

    private Biller biller;
    private Receipt receipt;
    private List<Product> productList;
    private String country;

    public PaymentCounter(String country) {
        this.country = country;
    }

    public void billItemsInCart(ShoppingCart cart) {
        productList = cart.getItemsFromCart();

        for (Product product : productList) {
            biller = getBiller(country);
            double productTax = biller.calculateTax(product.getPrice(), product.getTaxValue(country), product.isImported(), product.isExempted());
            double taxedCost = biller.calcTotalProductCost(product.getPrice(), productTax);

            product.setTaxedCost(taxedCost);
        }
    }

    public Receipt getReceipt() {
        double totalTax = biller.calcTotalTax(productList);
        double totalAmount = biller.calcTotalAmount(productList);
        receipt = biller.createNewReceipt(productList, totalTax, totalAmount);
        return receipt;
    }

    public void printReceipt(Receipt receipt) {
        biller.generateReceipt(receipt);
    }

    public Biller getBiller(String strategy) {
        TaxCalculatorFactory factory = new TaxCalculatorFactory();
        ITaxCalculator taxCal = factory.getTaxCalculator(strategy);
        return new Biller(taxCal);
    }
}
