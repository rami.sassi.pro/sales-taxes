package com.risf.salestaxes.billing;


import com.risf.salestaxes.products.Product;

import java.util.List;

public class Receipt {
    private List<Product> productList;
    private double totalSalesTax;
    private double totalAmount;

    public Receipt(List<Product> prod, double tax, double amount) {
        productList = prod;
        totalSalesTax = tax;
        totalAmount = amount;
    }

    public double getTotalSalesTax() {
        return totalSalesTax;
    }

    public double getTotalAmount() {
        return totalAmount;
    }


    @Override
    public String toString() {
        StringBuilder receipt = new StringBuilder();
        System.out.println("----------------------");
        for (Product product : productList) {
            receipt.append(product.toString()).append("\n");
        }

        receipt.append("Total sales tax = ").append(totalSalesTax).append("\n");
        receipt.append("Total amount = ").append(totalAmount).append("\n");

        return receipt.toString();
    }
}
