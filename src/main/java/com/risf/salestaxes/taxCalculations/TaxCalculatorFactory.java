package com.risf.salestaxes.taxCalculations;

import java.util.HashMap;
import java.util.Map;

public class TaxCalculatorFactory {
    private Map<String, ITaxCalculator> taxCalculators;

    public TaxCalculatorFactory() {
        taxCalculators = new HashMap<>();
        registerInFactory("Local", new LocalTaxCalculator());
    }

    public void registerInFactory(String strategy, ITaxCalculator taxCalc) {
        taxCalculators.put(strategy, taxCalc);
    }

    public ITaxCalculator getTaxCalculator(String strategy) {
        return taxCalculators.get(strategy);
    }

    public int getFactorySize() {
        return taxCalculators.size();
    }
}
