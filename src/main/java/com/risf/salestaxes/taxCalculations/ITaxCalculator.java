package com.risf.salestaxes.taxCalculations;

public interface ITaxCalculator {

    /**
     * Calculates Tax for a Product where Tax Cost is the Sum of Sales
     * Tax and Imported Duty of a Product.
     *
     * @param price The Price of the Product.
     * @param tax The Tax Rate of the Product.
     * @param imported Product is whether Imported or not.
     * @return The calculated tax.
     */
    double calculateTax(double price, double tax, boolean imported, boolean exempted);
}
