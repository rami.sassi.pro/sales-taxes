package com.risf.salestaxes.taxCalculations;

import com.risf.salestaxes.utils.TaxUtil;

public class LocalTaxCalculator implements ITaxCalculator {

    @Override
    public double calculateTax(double price, double localTax, boolean imported, boolean exempted) {
        double tax = 0.0;

        if (!imported && !exempted)
            // tax: 10%
            tax = (price * 0.1);

        if (imported && exempted)
            // tax: 5%
            tax = (price * 0.05);

        if (imported && !exempted)
            // tax: 15%
            tax = (price * 0.15);

        // Rounds off to nearest 0.05
        tax = TaxUtil.roundOff(tax);

        return tax;
    }
}
